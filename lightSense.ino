//Värdet som fotoresistorkretsen ger oss
int sensorValue;

//(sensorLow och sensorHigh) kommer att modifieras i setup funktionen)
//Lägsta värdet vi kan få från fotoresistorkretsen
int sensorLow = 1023;
//Högsta värdet vi kan få från fotoresistorkretsen
int sensorHigh = 0;

void setup() {
	/* Vi will att led-lampan på pin 13 ska lysa tills
	 * dess att setup funktionen har hittat lägsta och högsta
	 * värdet som fotoresistorkretsen ger oss
	 */
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);

	/* Följande kod känner av högsta och lägsta värdet
	 * som fotoresistorkretsen ger ifrån sig under 
	 * 5 sekunder, dessa värden blir sparade i
	 * sensorHigh och sensorLow.
	 *
	 * Viktigt att man både täcker fotoresistor så mkt som möjligt
	 * en stund under dessa 5 sekunder, samt att man låter den 
	 * vara fri från skugga en stund.
	 */
  while (millis() < 5000) {
    sensorValue = analogRead(A0);
    if (sensorValue > sensorHigh) {
      sensorHigh = sensorValue;
    }
    if (sensorValue < sensorLow) {
      sensorLow = sensorValue;
    }
  }

	/* Vi har nu hittat lägsta och högsta värdet, så nu släcker vi 
	 * led lampan på pin 13
	 */
  digitalWrite(13, LOW);
}

void loop() {

	//Här läser vi in värdet från fotoresistorkretsen
  sensorValue = analogRead(A0);

  /* Vi mappar om värdet från fotoresistorkretsen till ett värde som
   * går mellan 0 och 255, vilket är värden som analogWrite senare
   * kan te emot
   */
  int lightIntensity = map(sensorValue, sensorLow, sensorHigh, 0, 255);
  
  //Här sätter vi vilken intensitet som led-lampan på port 11 ska lysa med
  analogWrite(11, lightIntensity);

  delay(10);
}
